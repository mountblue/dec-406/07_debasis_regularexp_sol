# Q 36
import re
def camel_to_snake(text):
        str1 = re.sub('([A-Z][a-z]+)(.)', r'\1_\2', text).lower()
        return str1
print(camel_to_snake('PythonExercises'))