# Q 37
import re
def snake_to_camel(text):
        ls = re.findall('[a-zA-Z0-9]+', text)
        camelCase = ""
        for word in ls:
            camelCase += word.capitalize()
        return camelCase
print(snake_to_camel('python_exercises'))