# Q 46
import re
def find_adverbs(string):
    pattern = "\w+ly"
    values = re.finditer(pattern, string)
    for adverbs in values:
        print(adverbs.group(), adverbs.span())
find_adverbs("Clearly, he has no excuse for nicely such behavior.")