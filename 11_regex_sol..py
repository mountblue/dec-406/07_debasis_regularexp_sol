import re
def text_match(text):
        patterns = 'hello(!)?$'
        if re.search(patterns,  text):
                return 'Found a match!'
        else:
                return('Not matched!')

print(text_match("aab_cbbbc"))
print(text_match("aab_Abbbchello!"))
print(text_match("hellohhdhello"))