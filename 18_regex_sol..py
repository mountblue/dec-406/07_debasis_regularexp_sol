import re
def text_match(text):
        patterns = '([0-9]{1,3})'
        if re.search(patterns,  text):
                return 'Found a match!'
        else:
                return('Not matched!')
print(text_match("23.000440.0540.00000"))
print(text_match("aab_zAbbb$cz0"))
print(text_match("zhelzlohhdhz"))