import re
def text_match(text):
        patterns = '^[a-yA-Y]+z[a-yA-Y]+$'
        if re.search(patterns,  text):
                return 'Found a match!'
        else:
                return('Not matched!')

print(text_match("ehzll"))
print(text_match("aab_zAbbbcz"))
print(text_match("zhelzlohhdhz"))