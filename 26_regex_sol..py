import re
def search(list_of_words):
        pattern = '[P]\w+'
        find_match = []
        for word in list_of_words:
            if re.search(pattern,word):
                find_match.append(word)
                
        if len(find_match) >= 2:
            return find_match
        else:
            return "Sorry there is no or few word to match the pattern"
print(search(["hello", "pello", "hhdhd","Pehhhs","hirse"]))