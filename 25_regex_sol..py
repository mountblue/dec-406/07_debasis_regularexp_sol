import re
def convert_date(date):
        pattern = '((\d{2,4})/(\d{1,2})/(\d{1,2}))'
        values = re.findall(pattern, date) 
        return "{}/{}/{}".format(values[0][3],values[0][2],values[0][1])
print(convert_date("2017/3/21"))