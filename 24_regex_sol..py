import re
def extract_date(url):
        pattern = '/((\d{1,2})/(\d{1,2})/(\d{2,4}))'
        values = re.findall(pattern, url) 
        return values[0][0]
print(extract_date("www.hello.com/api/2/3/2017"))