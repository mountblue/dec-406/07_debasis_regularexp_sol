# Q 51
import re

def add_spaces(string):
    pattern = re.compile(r"[A-Z][a-z]*")
    values = re.findall(pattern,string)
    print(" ".join(values))

add_spaces("HelloThreHowAreYou")