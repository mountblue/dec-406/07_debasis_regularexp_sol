# Q 42
import re
def find_urls(string):
    pattern = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    urls = re.findall(pattern, string)
    return urls
find_urls("https://dashboard.heroku.com/")